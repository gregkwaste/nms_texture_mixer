﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
using OpenTK.Graphics.OpenGL;
using OpenTK.Math;
using KUtility;
using GLSL_Preproc;

namespace TextureMixer
{
    public partial class Form1 : Form
    {
        private bool glloaded;
        private int shader_program;

        //QUAD GEOMETRY
        private float[] quad = new float[6 * 3] {
                -1.0f, -1.0f, 0.0f,
                1.0f, -1.0f, 0.0f,
                -1.0f,  1.0f, 0.0f,
                -1.0f,  1.0f, 0.0f,
                1.0f, -1.0f, 0.0f,
                1.0f,  1.0f, 0.0f };

        private float[] quadcolors = new float[6 * 3]
        {
                1.0f, 0.0f, 0.0f,
                0.0f, 1.0f, 0.0f,
                1.0f,  1.0f, 0.0f,
                1.0f,  1.0f, 0.0f,
                0.0f, 1.0f, 0.0f,
                0.0f,  0.0f, 1.0f
        };

        //Indices
        private int[] indices = new Int32[2 * 3] { 0, 1, 2, 3, 4, 5 };

        private int quad_vbo, quad_ebo;

        //TEXTURE INFO

        private List<Texture> diffTextures = new List<Texture>(8);
        private List<Texture> maskTextures = new List<Texture>(8);
        private float[] baseLayersUsed = new float[8];
        private float[] alphaLayersUsed = new float[8];
        private List<float[]> reColours = new List<float[]>(8);

        //Default Textures
        private Texture dDiff;
        private Texture dMask;

        //Rendering Options
        private float hasAlphaChannel = 0.0f;
        private int  renderMode = 0;

        public Form1()
        {
            InitializeComponent();

            //CheckBoxes
            this.diffuseList.MouseUp += new System.Windows.Forms.MouseEventHandler(this.List_RightClick);
            this.diffuseList.MouseDown += new System.Windows.Forms.MouseEventHandler(this.List_SelectOnClick);
            this.alphaList.MouseUp += new System.Windows.Forms.MouseEventHandler(this.List_RightClick);
            this.alphaList.MouseDown += new System.Windows.Forms.MouseEventHandler(this.List_SelectOnClick);

            //GL Control
            this.glControl1.Load += new System.EventHandler(this.glControl_Load);
            this.glControl1.Paint += new System.Windows.Forms.PaintEventHandler(this.glControl1_Paint);
            this.glControl1.Resize += new System.EventHandler(this.glControl1_Resize);

            //Init the Texture lists
            for (int i = 0; i < 8; i++)
            {
                diffTextures.Add(null);
                maskTextures.Add(null);
                baseLayersUsed[i] = 0.0f;
                alphaLayersUsed[i] = 0.0f;
                reColours.Add(new float[] { 1.0f, 1.0f, 1.0f, 0.0f });
            }

        }
        

        private void glControl1_Paint(object sender, PaintEventArgs e)
        {
            if (!this.glloaded)
                return;
            glControl1.MakeCurrent();
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
            //GL.ClearColor(System.Drawing.Color.Black);
            glControl1_Render();
            glControl1.SwapBuffers();
            //translate_View();
            ////Draw scene
            //GL.MatrixMode(MatrixMode.Modelview);
            //Update Joystick 

            //glControl1.Invalidate();
            //Debug.WriteLine("Painting Control");
        }

        private void glControl_Load(object sender, EventArgs e)
        {
            GL.Viewport(0, 0, glControl1.ClientSize.Width, glControl1.ClientSize.Height);
            GL.ClearColor(System.Drawing.Color.Black);
            GL.Enable(EnableCap.DepthTest);
            //glControl1.SwapBuffers();
            //glControl1.Invalidate();
            Debug.WriteLine("GL Cleared");
            Debug.WriteLine(GL.GetError());

            this.glloaded = true;

            //Generate Geometry VBOs
            GL.GenBuffers(1, out quad_vbo);
            GL.GenBuffers(1, out quad_ebo);

            //Bind Geometry Buffers
            int arraysize = sizeof(float) * 6 * 3;
            //Upload vertex buffer
            GL.BindBuffer(BufferTarget.ArrayBuffer, quad_vbo);
            //Allocate to NULL
            GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(2 * arraysize), (IntPtr)null, BufferUsageHint.StaticDraw);
            //Add verts data
            GL.BufferSubData(BufferTarget.ArrayBuffer, (IntPtr)0, (IntPtr)arraysize, quad);
            //Add color data
            GL.BufferSubData(BufferTarget.ArrayBuffer, (IntPtr)arraysize, (IntPtr)arraysize, quadcolors);

            //Upload index buffer
            GL.BindBuffer(BufferTarget.ElementArrayBuffer, quad_ebo);
            GL.BufferData(BufferTarget.ElementArrayBuffer, (IntPtr)(sizeof(int) * 6), indices, BufferUsageHint.StaticDraw);


            //Compile Shaders
            string vvs, ffs;
            int vertex_shader_ob, fragment_shader_ob;
            vvs = GLSL_Preprocessor.Parser("Shaders/pass_VS.glsl");
            ffs = GLSL_Preprocessor.Parser("Shaders/pass_FS.glsl");
            //Compile Texture Shaders
            CreateShaders(vvs, ffs, out vertex_shader_ob,
                    out fragment_shader_ob, out shader_program);


            //Setup default program
            GL.UseProgram(shader_program);

            //Init Default Textures
            dDiff = new Texture("default.dds");
            dMask = new Texture("default_mask.dds");


            glControl1.Invalidate();
        }

        private void glControl1_Resize(object sender, EventArgs e)
        {
            if (!this.glloaded)
                return;
            if (glControl1.ClientSize.Height == 0)
                glControl1.ClientSize = new System.Drawing.Size(glControl1.ClientSize.Width, 1);
            Debug.WriteLine("GLControl Resizing");
            GL.Viewport(0, 0, glControl1.ClientSize.Width, glControl1.ClientSize.Height);
        }


        private void glControl1_Render()
        {
            Debug.WriteLine("Rendering");

            // Attach to Shaders
            int vpos, cpos;
            int arraysize = sizeof(float) * 6 * 3;
            //Vertex attribute
            //Bind vertex buffer
            GL.BindBuffer(BufferTarget.ArrayBuffer, quad_vbo);
            vpos = GL.GetAttribLocation(shader_program, "vPosition");
            GL.VertexAttribPointer(vpos, 3, VertexAttribPointerType.Float, false, 0, 0);
            GL.EnableVertexAttribArray(vpos);

            cpos = GL.GetAttribLocation(shader_program, "vColor");
            GL.VertexAttribPointer(cpos, 3, VertexAttribPointerType.Float, false, 0, (IntPtr)arraysize);
            GL.EnableVertexAttribArray(cpos);

            //Bind elem buffer
            GL.BindBuffer(BufferTarget.ElementArrayBuffer, quad_ebo);

            //BIND TEXTURES
            Texture tex;
            int loc;

            //If there are samples defined, there are diffuse textures for sure

            //GL.Enable(EnableCap.Blend);
            //GL.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);

            //NEW WAY OF TEXTURE BINDING

            //DIFFUSE TEXTURES
            //GL.Enable(EnableCap.Blend);
            //GL.BlendFunc(BlendingFactorSrc.ConstantAlpha, BlendingFactorDest.OneMinusConstantAlpha);

            //Upload base Layers Used
            for (int i = 0; i < 8; i++)
            {
                loc = GL.GetUniformLocation(shader_program, "lbaseLayersUsed[" + i.ToString() + "]");
                GL.Uniform1(loc, baseLayersUsed[i]);
            }

            for (int i = 0; i < 8; i++)
            {

                if (diffTextures[i] != null)
                    tex = diffTextures[i];
                else
                    tex = dMask;
                
                //Upload diffuse Texture
                string sem = "diffuseTex[" + i.ToString() + "]";
                //Get Texture location
                loc = GL.GetUniformLocation(shader_program, sem);
                GL.Uniform1(loc, i); // I need to upload the texture unit number

                int tex0Id = (int)TextureUnit.Texture0;

                //Upload hasAlpha
                sem = "hasAlphaChannel[" + i.ToString() + "]";
                loc = GL.GetUniformLocation(shader_program, sem);
                if (tex.containsAlphaMap)
                    GL.Uniform1(loc, 1.0f);
                else
                    GL.Uniform1(loc, 0.0f);

                //Upload PaletteColor
                //loc = GL.GetUniformLocation(pass_program, "palColors[" + i.ToString() + "]");
                //Use Texture paletteOpt and object palette to load the palette color
                //GL.Uniform3(loc, palette[tex.palOpt.PaletteName][tex.palOpt.ColorName]);

                GL.ActiveTexture((OpenTK.Graphics.OpenGL.TextureUnit)(tex0Id + i));
                GL.BindTexture(TextureTarget.Texture2D, tex.bufferID);
                
            }

            //TESTING MASKS
            //SETTING HASALPHACHANNEL FLAG TO FALSE
            loc = GL.GetUniformLocation(shader_program, "hasAlphaChannel");
            GL.Uniform1(loc, hasAlphaChannel);

            //Toggle mask-diffuse
            loc = GL.GetUniformLocation(shader_program, "mode");
            GL.Uniform1(loc, renderMode);


            //MASKS
            //Upload alpha Layers Used
            for (int i = 0; i < 8; i++)
            {
                loc = GL.GetUniformLocation(shader_program, "lalphaLayersUsed[" + i.ToString() + "]");
                GL.Uniform1(loc, alphaLayersUsed[i]);
            }

            //Upload Mask Textures -- Alpha Masks???
            for (int i = 0; i < 8; i++)
            {
                if (maskTextures[i] != null)
                    tex = maskTextures[i];
                else
                    tex = dDiff;
                

                //Upload diffuse Texture
                string sem = "maskTex[" + i.ToString() + "]";
                //Get Texture location
                loc = GL.GetUniformLocation(shader_program, sem);
                GL.Uniform1(loc, 8 + i); // I need to upload the texture unit number

                int tex0Id = (int)TextureUnit.Texture0;

                //Upload PaletteColor
                //loc = GL.GetUniformLocation(pass_program, "palColors[" + i.ToString() + "]");
                //Use Texture paletteOpt and object palette to load the palette color
                //GL.Uniform3(loc, palette[tex.palOpt.PaletteName][tex.palOpt.ColorName]);

                GL.ActiveTexture((OpenTK.Graphics.OpenGL.TextureUnit)(tex0Id + 8 + i));
                GL.BindTexture(TextureTarget.Texture2D, tex.bufferID);

            }

            //Upload Recolouring Information
            for (int i = 0; i < 8; i++)
            {
                loc = GL.GetUniformLocation(shader_program, "lRecolours[" + i.ToString() + "]");
                GL.Uniform4(loc, reColours[i][0], reColours[i][1], reColours[i][2], reColours[i][3]);
            }


            //RENDERING PHASE
            //GL.Enable(EnableCap.Blend);
            //GL.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);
            GL.DrawArrays(PrimitiveType.Triangles, 0, 6);

            GL.DisableVertexAttribArray(vpos);
            GL.DisableVertexAttribArray(cpos);
        }

        //Context Menus on ListBox
        private void List_RightClick(object sender, MouseEventArgs e)
        {

            ListView lbox = (ListView)sender;

            //Test the click
            var info = lbox.HitTest(e.X, e.Y);

            if (info.Item != null)
            {
                var row = info.Item.Index;
                var col = info.Item.SubItems.IndexOf(info.SubItem);

                if (e.Button == MouseButtons.Right)
                {
                    contextMenuStrip1.lBox = lbox;
                    contextMenuStrip1.Show(Cursor.Position);
                }

            }

            
        }

        private void List_SelectOnClick(object sender, MouseEventArgs e)
        {
            ListView lbox = (ListView) sender;

            //Test the click
            var info = lbox.HitTest(e.X, e.Y);

            //For if the click was on an item select it
            foreach (ListViewItem item in lbox.Items)
                item.Selected = false;

            if (info.Item != null)
            {
                var row = info.Item.Index;
                var col = info.Item.SubItems.IndexOf(info.SubItem);

                info.Item.Selected = true;
            }

        }

        private void importTexturesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Debug.WriteLine("Importing Texture from here");

            ToolStripMenuItem sitem = ((ToolStripMenuItem) sender);
            ListView lbox = ((customStrip) sitem.Owner).lBox;

            Debug.WriteLine(lbox.SelectedItems);

            int index = lbox.SelectedIndices[0];

            DialogResult res = openFileDialog1.ShowDialog();

            if (res != DialogResult.OK)
                return;

            //Load the texture 
            Texture tex = new Texture(openFileDialog1.FileName);


            //Destroy texture if i'm about to replace
            if (lbox.Name.Contains("diffuse"))
            {
                if (diffTextures[index] != null)
                    GL.DeleteTexture(diffTextures[index].bufferID);

                diffTextures[index] = tex;
                lbox.Items[index].Text = tex.name;

                baseLayersUsed[index] = 1.0f;
            }
            else
            {
                if (maskTextures[index] != null)
                    GL.DeleteTexture(maskTextures[index].bufferID);

                maskTextures[index] = tex;
                lbox.Items[index].Text = tex.name;

                alphaLayersUsed[index] = 1.0f;
            }
                
            glControl1.Invalidate();
        }

        private void setReColourToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Debug.WriteLine("Setting ReColor");

            ToolStripMenuItem sitem = ((ToolStripMenuItem)sender);
            ListView lbox = ((customStrip)sitem.Owner).lBox;

            Debug.WriteLine(lbox.SelectedItems);

            int index = lbox.SelectedIndices[0];

            //Color Dialog
            ColorDialog coldiag = new ColorDialog();

            DialogResult res = coldiag.ShowDialog();
            if (res == DialogResult.OK)
            {
                for (int i = 0; i < 8; i++)
                {
                    lbox.Items[i].BackColor = coldiag.Color;
                    reColours[i] = new float[] { (int)coldiag.Color.R / 256.0f, (int)coldiag.Color.G / 256.0f, (int)coldiag.Color.B / 256.0f, 1.0f };
                }
                
            }
            else
            {
                lbox.Items[index].BackColor = System.Drawing.Color.White;
                reColours[index] = new float[] { 1.0f, 1.0f, 1.0f, 0.0f };
            }

            Debug.WriteLine(reColours[index][0]);
            glControl1.Invalidate();

        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox chk = (CheckBox)sender;

            if (chk.Checked)
                this.hasAlphaChannel = 1.0f;
            else
                this.hasAlphaChannel = 0.0f;

            glControl1.Invalidate();
        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox chk = (CheckBox)sender;

            if (chk.Checked)
                this.renderMode = 1;
            else
                this.renderMode = 0;

            glControl1.Invalidate();
        }


        //Shader Creation
        private void CreateShaders(string vs, string fs, out int vertexObject,
            out int fragmentObject, out int program)
        {
            int status_code;
            string info;

            vertexObject = GL.CreateShader(ShaderType.VertexShader);
            fragmentObject = GL.CreateShader(ShaderType.FragmentShader);

            //Compile vertex Shader
            GL.ShaderSource(vertexObject, vs);
            GL.CompileShader(vertexObject);
            GL.GetShaderInfoLog(vertexObject, out info);
            GL.GetShader(vertexObject, ShaderParameter.CompileStatus, out status_code);
            if (status_code != 1)
                throw new ApplicationException(info);

            //Compile fragment Shader
            GL.ShaderSource(fragmentObject, fs);

            //HANDLE INCLUDES
            GL.CompileShader(fragmentObject);
            GL.GetShaderInfoLog(fragmentObject, out info);
            GL.GetShader(fragmentObject, ShaderParameter.CompileStatus, out status_code);
            if (status_code != 1)
                throw new ApplicationException(info);

            program = GL.CreateProgram();
            GL.AttachShader(program, fragmentObject);
            GL.AttachShader(program, vertexObject);
            GL.LinkProgram(program);
            //GL.UseProgram(program);

        }
    }

    public class Texture
    {
        public int bufferID;
        public string name;
        public int width;
        public int height;
        public PixelInternalFormat pif;
        public bool containsAlphaMap;
        public Vector3 procColor;
        public PixelFormat pf;
        public DDSImage ddsImage;
        //Attach mask and normal textures to the diffuse
        public Texture mask;
        public Texture normal;

        //Empty Initializer
        public Texture() { }
        //Path Initializer
        public Texture(string path)
        {
            DDSImage dds;
            if (!File.Exists(path))
                throw new System.IO.FileNotFoundException();

            ddsImage = new DDSImage(File.ReadAllBytes(path));
            name = path;
            Debug.WriteLine("Sampler Name Path " + path + " Width {0} Height {1}", ddsImage.header.dwWidth, ddsImage.header.dwHeight);
            width = ddsImage.header.dwWidth;
            height = ddsImage.header.dwHeight;
            switch (ddsImage.header.ddspf.dwFourCC)
            {
                //DXT1
                case (0x31545844):
                    pif = PixelInternalFormat.CompressedRgbaS3tcDxt1Ext;
                    containsAlphaMap = false;
                    break;
                case (0x35545844):
                    pif = PixelInternalFormat.CompressedRgbaS3tcDxt5Ext;
                    containsAlphaMap = true;
                    break;
                default:
                    throw new ApplicationException("Unimplemented Pixel format");
            }
            //Force RGBA for now
            pf = PixelFormat.Rgba;
            //Upload to GPU
            bufferID = GL.GenTexture();
            GL.BindTexture(TextureTarget.Texture2D, bufferID);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapS, (int)TextureWrapMode.Repeat);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapT, (int)TextureWrapMode.Repeat);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Linear);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Linear);

            GL.CompressedTexImage2D(TextureTarget.Texture2D, 0, this.pif,
                this.width, this.height, 0, this.ddsImage.header.dwPitchOrLinearSize, this.ddsImage.bdata);

        }

    }

    public class customStrip: System.Windows.Forms.ContextMenuStrip
    {
        public ListView lBox;
        
        public customStrip(System.ComponentModel.IContainer components) : base(components)
        {
            
        }
    }
}
